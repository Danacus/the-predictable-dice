#!/bin/env python

import numpy as np
import os
import time
import random

import matplotlib.pyplot as plt
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)
from matplotlib.figure import Figure

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

## Config

max_value = 6
dice = 2
graph = os.getenv('DICE_GRAPH')


## Probably Probability Stuff

choices = list(range(0, max_value * dice + 1))
weights = [0] * 2 + [1 / (max_value * dice)] * (max_value * dice - 1)
history = [0] * (max_value * dice + 2)
history_normal = [0] * (max_value * dice + 2)


def generate(max_value, w):
    rnd = np.random.choice(choices, p=normalize([dice_prob(i) * x for i, x in enumerate(w)]))

    n = 4
    w[rnd] /= n

    add = [0] + [(n - 1) * w[rnd] / (n * max_value * dice - 1)] * (max_value * dice)
    add = normalize([dice_prob(i) * x for i, x in enumerate(add)])
    
    for j in choices:
        if j != rnd:
            w[j] += add[j]

    history[rnd] += 1

    return rnd

def generate_normal(max_value):
    val = random.randint(0, max_value - 1)
    return val

def dice_prob(x):
    n = 6

    # Make 7 less powerful
    #if x == 7:
        #return 0.1

    if x < 2 or x > 2 * n:
        return 0
    else:
        return (n - abs(x - (n + 1))) / n**2
        
    # Use a completely different distribution
    #return x**2 / 576

def normalize(l):
    return [float(x) / sum(l) for x in l]


## GTK+ Stuff

def set_text(val):
    builder.get_object("dice_label").set_text(str(val))

def draw_graph():
    a.clear()
    p = normalize([dice_prob(i) * x for i, x in enumerate(weights)]) + [0.5]
    a.bar(np.arange(len(p)), p, width = [0.8] * (len(p) - 1) + [0])
    f.canvas.draw()


class Handler:
    def onDestroy(self, *args):
        Gtk.main_quit()

    def onButtonPressed(self, button):
        val = generate(max_value, weights)
        set_text(val)
        draw_graph()

builder = Gtk.Builder()
builder.add_from_file("window.glade")
builder.connect_signals(Handler())


f = Figure(figsize=(5, 4), dpi=100)
a = f.add_subplot(111)
draw_graph()

box = builder.get_object("graph_box")

canvas = FigureCanvas(f)  # a Gtk.DrawingArea
canvas.set_size_request(800, 500)

if graph:
    box.add(canvas)

win = builder.get_object("window")
win.show_all()

Gtk.main()

plt.plot(history)
plt.plot([sum(history) * dice_prob(x) for x in choices])
plt.show()


## Testing Stuff

"""

std_cool = 0
std_normal = 0

num_exp = 100
num_tries = 1

results = []
results_normal = []

for i in range(num_tries):

    max_value = 6
    dice = 2

    choices = list(range(0, max_value * dice + 1))
    weights = [0] + [1 / (max_value * dice)] * (max_value * dice)
    history = [0] * (max_value * dice + 2)
    history_normal = [0] * (max_value * dice + 2)

    for i in range(0, num_exp):
        val = generate(max_value, weights)
        print(val)
        results.append(val)
        history[val] += 1

    print("Cool method: ", history)
    plt.subplot(121)
    plt.plot(history)
    plt.plot([num_exp * dice_prob(x) for x in choices])
    plt.ylabel('Cool method')

    print(np.std(history))
    std_cool += np.std(history)

    for i in range(0, num_exp):
        result = 0

        for d in range(dice):
            val = generate_normal(max_value) + 1
            result += val

        print(result)
        results_normal.append(result)
        history_normal[result] += 1

    print("Normal method: ", history_normal)
    plt.subplot(122)
    plt.plot(history_normal)
    plt.plot([num_exp * dice_prob(x) for x in choices])
    plt.ylabel('Normal method')
    plt.show()
    print(np.std(history_normal))
    std_normal += np.std(history_normal)

plt.subplot(121)
plt.plot(results)
plt.subplot(122)
plt.plot(results_normal)
plt.show()

print(std_cool / 100)
print(std_normal / 100)

"""
